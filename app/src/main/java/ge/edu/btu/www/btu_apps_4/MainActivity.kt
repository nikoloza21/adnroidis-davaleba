package ge.edu.btu.www.btu_apps_4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init() {
        submitButton.setOnClickListener {
            if (userEmail.text.isNotEmpty() && userPassword.text.isNotEmpty()) {
                Toast.makeText(this, "სწორია", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "შეავსეთ ყველა ველი",  Toast.LENGTH_SHORT).show()
            }
        }
    }
}
